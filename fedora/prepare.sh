#!/bin/sh

set -e

# Install what we can from DNF.
dnf install -y dbus-x11 dconf glib2-devel libmediaart-devel meson python3-gobject python3-pytest python3-pytest-benchmark python3-pyyaml rsync tracker3-devel tracker3-miners

# Some of the Tracker tests fail if we set LANG=C.UTF-8 LC_ALL=C.UTF-8. Setting
# LANG=en_US.UTF-8 produces a warning, unless this package is installed. (The
# tests seem to pass either way, though).
dnf install -y glibc-langpack-en.x86_64

# Install the remaining deps from PyPI.
#
# Some dependencies contain C code, so we need a C toolchain available during this process.
# Non exhaustive list: splitstream
dnf install -y gcc python3-devel python3-pip redhat-rpm-config

pip3 install click jsonschema parsedatetime splitstream yoyo-migrations
pip3 install mutagen
pip3 install cachecontrol google-api-python-client jinja2 lastfmclient lockfile musicbrainzngs simpleai spotipy
dnf install -y gstreamer1-plugins-good

# Documentation dependencies
dnf install -y python3-sphinx python3-sphinx-click
pip3 install sphinx-autoapi sphinx-jsonschema

# The version of pylint packaged by Fedora is too old, at time of writing.
# We need 2.2.x to ensure Python 3.6 support.
pip3 install pylint

pip3 install pytest-pep8

# jinja2 requires python3-markupsafe, this prevents it being removed during cleanup.
dnf mark install python3-markupsafe

# spotipy requires python3-idna, this prevents it being removed during cleanup
dnf mark install python3-idna

# dbus-launch warns if there's no /etc/machine-idwarns if there's no /etc/machine-id..
systemd-machine-id-setup
